package assign1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.*;

/**
 * @author rhad2518
 */

public class SocialNetwork {
	// Put your instance variables here
	// Store your collection of Person objects here.
	ArrayList<Person> sn = new ArrayList<Person>();
	public SocialNetwork() {
		// TODO instantiate the data collection here to a sensible default state
		sn = new ArrayList<Person>();
	}
	public SocialNetwork(ArrayList<Person> p) {
		// TODO instantiate the data collection here
		for (Person person:p){
			addPerson(person);
		}
	}
	public void addPerson(Person p) {
		if (!sn.contains(p)) {
			sn.add(p);
		}
	}
	public boolean areFriends(Person p, Person q) {
		// TODO return true iff p and q are each other's friends'
		return (p.isFriendsWith(q) && q.isFriendsWith(p));
	}
	public void clear() {
		this.sn.clear();
		// TODO empty the collection of all data
	}
	public void destroyFriendship(Person p, Person q) {
		// TODO destroy the friendship of p and q
		if (areFriends(p,q)) {
			p.removeFriend(q.getName());
			q.removeFriend(p.getName());
		}
	}
	
	
	public boolean greaterThan5Years(Person p1, Person p2){
		return Math.abs(p1.getyearOfBirth() - p2.getyearOfBirth()) <= 5;
	}
	
	public ArrayList<Person> findMatches(Person p) {
		/* 
		 * TODO return a collection of all the people with
		 * (a) at least one interest in common
		 * (b) birthdates at most 5 years apart
		 */
		ArrayList<Person> matches = new ArrayList<Person>();
		ArrayList<String> person_interest = p.getInterest();
		for (Person individual:this.sn) { //for every indivdual in the social network
			ArrayList<String> interests = individual.getInterest();//get a list of he/her integerest
			for (String interest: interests) {//for every string in the list of interest
				if (person_interest.contains(interest) &&  greaterThan5Years(p, individual)){//check if it contains with the passed person objects interest
					if (!matches.contains(individual) && (individual != p)){
						matches.add(individual);//if so add it to the array
					}
				}
			}
			
			
		}
		return matches; // stub
	}

	
	
	class Node {
		public Person person;
		public int depth;
		public Node(Person person, int depth){
			this.person = person;
			this.depth = depth;
		}
	}
	
	public int getDegreesOfSeparation(Person p, Person q) {
		Stack<Node> path = new Stack<Node>();
		Stack<Person> visited = new Stack<Person>(); 
		path.add(new Node(p,0));
		while (!path.isEmpty()) {
			Node current = path.pop();
			if (current.person == q) {
				return current.depth;
			} else {
				visited.push(current.person);
 
				ArrayList<Person> connections = getConnections(current.person);
				for(Person person:connections) {
					current = new Node(person,current.depth +1);
					if (!visited.contains(current.person)) {
						path.push(current);
					}
				}
			}
		}
		return -1;
	}
	
	
	
	public ArrayList<Person> getFriendsInCommon(Person p, Person q) {
		ArrayList<Person> friendsInCommon = new ArrayList<Person>();
		ArrayList<String> qfriends = q.getFriendsNames();
		ArrayList<String> pfriends = p.getFriendsNames();
		for (String friend: qfriends) {
			if (pfriends.contains(friend)) {
				friendsInCommon.add(getPerson(friend));
			}
		}
		return friendsInCommon;
	}
	public ArrayList<Person> getPeople() {
		// TODO return an array list of all the people in the collection
		return this.sn;
	}
	public ArrayList<Person> getPeopleWithInterest(String interest) {
		// TODO return a list of all the people with the interest given
		ArrayList<Person> people = new ArrayList<Person>();
		for (Person person:this.sn) {
			for (String inter:person.getInterest()){
				if (inter.equalsIgnoreCase(interest)) {
					people.add(person);
				}
			}
		}
		return people;
	}
	
	
	// write a data structure here to store who's in the network
	public Person getPerson(String str) {
		// TODO complete this method
		// Get the person from the collection whose name is this String
		for (Person person: this.sn) {
			if (person.getName().equalsIgnoreCase(str)) {
				return person;
			}
			
		}
		return null;
	}
	public int getSize() {
		// TODO return the number of people (Person objects) in the collection
		return this.sn.size(); // stub
	}
	
	
	public boolean directConnected(Person p,Person q) {
		return (p.isFriendsWith(q) || q.isFriendsWith(p));
	}

	public ArrayList<Person> getConnections(Person p) {
		ArrayList<Person> connections = new ArrayList<Person>();
		for (Person person:this.sn) {
			if (person.getFriendsNames().contains(p.getName()) || p.getFriendsNames().contains(person.getName())) {
				if (!connections.contains(person))
					connections.add(person);
			}
		}
		return connections;
	}


	public boolean isConnected(Person p, Person q) {
		if(directConnected(p, q)) {
			return true;
		}
		ArrayList<Person> connections = getConnections(p);
		Collections.shuffle(connections);//so different orders are generated
		for (Person person: connections) {
			return isConnected(person,q);
		}
		return false;
	}
		
	
	
	public boolean isEmpty() {
		// TODO write a method that returns a true if and only if the collection is empty
		return this.sn.isEmpty();
	}
	public void makeFriends(Person p, Person q) {
		p.addFriend(q.getName());
		q.addFriend(p.getName());
	}
}
