package assign1;

import java.util.ArrayList;

public interface Sociable {

	public void addFriend(String s); // add friend's name if not already present
	
	public void addFriends(String [] inFriends); // add a list of friends' names

	public void addInterest(String str); // add an interest, if not already there.

	public ArrayList<String> getFriendsNames();

	public String getName(); // return the name of the Sociable object

	public boolean isFriendsWith(Sociable p); // return true iff friends with p

	public void removeFriend(String s); // remove friend with name s
	
	public void removeInterest(String interest); // remove an interest if it's there
}
