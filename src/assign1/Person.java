package assign1;

/**
 * @author rhad2518
 */

import java.util.ArrayList;
import java.util.Arrays;

public class Person implements Sociable {
	private String name;
	private int yearOfBirth;
	private ArrayList<String> friends; // just their names stored here
	private ArrayList<String> interests;
	public Person(String inName, int yob, String[] inInterests) {
		// TODO fill this in
		this.friends = new ArrayList();
		this.name = inName.toLowerCase();
		this.yearOfBirth = yob;
		this.interests = new ArrayList(Arrays.asList(inInterests));
	}
	
	public void addFriend(String s){
		// add friend's name if not already present
		if (!this.friends.contains(s.toLowerCase())){
			this.friends.add(s.toLowerCase());
		}
	}
	
	public void addFriends(String [] inFriends){
		// add a list of friends' names]
		for (String friend: inFriends) {
			addFriend(friend.toLowerCase());
		}
		//this.friends = new ArrayList(Arrays.asList(inFriends));
	}

	public void addInterest(String str){
		// add an interest, if not already there.
		if (!this.interests.contains(str)){
			this.interests.add(str);
		}
	}

	public ArrayList<String> getFriendsNames(){
		return this.friends;
		
	}

	public String getName() {
		// return the name of the Sociable object
		return this.name;
	}

	public boolean isFriendsWith(Sociable p){
		// return true iff friends with p
		return friends.contains(p.getName().toLowerCase());
	}

	public void removeFriend(String s){
		// remove friend with name s
		if (this.friends.contains(s.toLowerCase())){
			this.friends.remove(s.toLowerCase());
		}
	}
	
	public void removeInterest(String interest){
		// remove an interest if it's there
		if (this.interests.contains(interest)){
			this.interests.remove(interest);
		}
	}
	
	public String toString() {
		return this.name;
	}
	public int getyearOfBirth() {
		return this.yearOfBirth;
	}
	
	//////testing only
	public ArrayList<String> getInterest() {
		return this.interests;
	}
	//////
}
