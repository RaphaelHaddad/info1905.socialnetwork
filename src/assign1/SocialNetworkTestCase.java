package assign1;


import org.junit.Before;
import java.util.*;
import junit.framework.TestCase;
public class SocialNetworkTestCase extends TestCase{

	String [] temp = {"poop","eating poop"};
	Person raph;
	Person chris;
	Person ally;
	Person olivia;
	Person ponting;
	Person knuth;
	Person scarlett;
	Person jebus;
	Person baz;
	Person dan;
	Person ben;
	
	ArrayList<Person> people = new ArrayList<Person>();
	
	@Before
	public void setUp() throws Exception {
		raph = new Person("Raph",1990,temp);
		chris = new Person("Chris", 1987, temp);
		ally = new Person("Ally", 1986, temp);
		olivia = new Person("olivia", 2008, temp);
		ponting = new Person("Ricky", 1974, temp);
		knuth = new Person("Donald", 1938, temp); 
		scarlett = new Person("Scarlett", 1984, temp);
		jebus = new Person("Jesus", 0, temp);
		baz = new Person("baz",1984,temp);
		dan = new Person("dan",1946,temp);
		ben = new Person("ben",1954,temp);
		people.add(raph);
		people.add(chris);
		people.add(ally);
		people.add(olivia);
		people.add(ponting);
		people.add(knuth);
		people.add(scarlett);
		people.add(jebus);
	}

	public final void testSocialNetwork () {
		SocialNetwork sn = new SocialNetwork(this.people);
		sn.addPerson(ponting);//check for doubles
		assertEquals(8,sn.getPeople().size());
		
	}
	
	public final void testareFriends() {
		SocialNetwork sn = new SocialNetwork(this.people);
		raph.addFriend(chris.getName());
		chris.addFriend(raph.getName());
		assertTrue(sn.areFriends(raph, chris));
		ally.addFriend(olivia.getName());
		assertFalse(sn.areFriends(ally, olivia));
		assertFalse(sn.areFriends(jebus,scarlett));
	}
	
	public final void testClear() {
		SocialNetwork sn = new SocialNetwork(this.people);
		sn.clear();
		assertEquals(0,sn.getPeople().size());
	}
	
	public final void testdestroyFriendship(){
		SocialNetwork sn = new SocialNetwork(this.people);
		raph.addFriend(chris.getName());
		chris.addFriend(raph.getName());
		sn.destroyFriendship(raph, chris);
		assertFalse(sn.areFriends(raph, chris));
	}
	
	public final void testfindMatches() {
		ArrayList<Person> snArray = new ArrayList<Person>();
		ArrayList<Person> exp = new ArrayList<Person>();
		String [] temp = {"jumping","bouncing"};
		raph = new Person("raph",1990,temp);
		knuth = new Person("knuth",1995,temp);
		olivia = new Person("Olivia",1996,temp);
		String [] temp2 = {"jooking","looking"};
		jebus = new Person("Jebus",1993,temp2);
		String[] temp3 = {"jumping","not-jumping"};
		scarlett = new Person("scarlett", 1993, temp3);
		ponting = new Person("Ponting",3006,temp);
		ally = new Person("ally",1985,temp);
		snArray.add(ponting);
		snArray.add(ally);
		snArray.add(scarlett);
		snArray.add(jebus);
		snArray.add(olivia);
		snArray.add(raph);
		snArray.add(knuth);
		SocialNetwork sn = new SocialNetwork(snArray);
		exp.add(ally);
		exp.add(scarlett);
		exp.add(knuth);
		assertEquals(exp,sn.findMatches(raph));
	}
	
	public final void testgetPerson() {
		SocialNetwork sn = new SocialNetwork(this.people);
		assertEquals("jesus",sn.getPerson("Jesus").getName());
		assertEquals("jesus",sn.getPerson("JESUS").getName());
		assertEquals(jebus,sn.getPerson("JESUS"));
		assertEquals(jebus,sn.getPerson("JEsus"));
	}
	
	
	public final void testfriendInCommon() {
		raph.addFriend("ally");
		chris.addFriend("Ally");
		knuth.addFriend("jesus");
		chris.addFriend("jesus");
		raph.addFriend("ricKy");
		chris.addFriend("ricky");
		SocialNetwork sn = new SocialNetwork(this.people);
		ArrayList<Person> exp = new ArrayList<Person>();
		exp.add(ally);
		exp.add(ponting);
		assertEquals(exp,sn.getFriendsInCommon(raph, chris));
		exp.clear();
		exp.add(jebus);
		assertEquals(exp,sn.getFriendsInCommon(knuth, chris));
		exp.clear();
		assertEquals(exp,sn.getFriendsInCommon(raph, knuth));
		
	}
	
	public final void testgetSize() {
		SocialNetwork sn = new SocialNetwork(this.people);
		assertEquals(8, sn.getSize());
	}
	
	public final void testgetInterest() {
		ArrayList<Person> snArray = new ArrayList<Person>();
		ArrayList<Person> exp = new ArrayList<Person>();
		String [] temp = {"jumping","bouncing"};
		raph = new Person("raph",1990,temp);
		knuth = new Person("knuth",1995,temp);
		olivia = new Person("Olivia",1996,temp);
		String [] temp2 = {"jooking","looking"};
		jebus = new Person("Jebus",1993,temp2);
		String[] temp3 = {"jumping","not-jumping"};
		scarlett = new Person("scarlett", 1993, temp3);
		ponting = new Person("Ponting",3006,temp);
		ally = new Person("ally",1985,temp);
		snArray.add(ponting);
		snArray.add(ally);
		snArray.add(scarlett);
		snArray.add(jebus);
		snArray.add(olivia);
		snArray.add(raph);
		snArray.add(knuth);
		SocialNetwork sn = new SocialNetwork(snArray);
		exp.add(ponting);
		exp.add(ally);
		exp.add(scarlett);
		exp.add(olivia);
		exp.add(raph);
		exp.add(knuth);
		assertEquals(exp,sn.getPeopleWithInterest("jumping"));
	}
	
	public final void testisConnected() {
		raph.addFriend("jesus");
		jebus.addFriend("ally");
		jebus.addFriend("scarlett");
		scarlett.addFriend("jesus");
		jebus.addFriend("olivia");
		olivia.addFriend("Donald");
		chris.addFriend("Olivia");
		baz.addFriend("chris");
		scarlett.addFriend("ben");
		raph.addFriend("dan");
		dan.addFriend("raph");
		SocialNetwork sn = new SocialNetwork(this.people);
		assertFalse(sn.isConnected(ponting, knuth));
		assertTrue(sn.isConnected(jebus,olivia));
		assertTrue(sn.isConnected(scarlett, jebus));
		assertTrue(sn.isConnected(raph,jebus));
		assertTrue(sn.isConnected(chris, olivia));
		assertTrue(sn.isConnected(raph,scarlett));
		assertTrue(sn.isConnected(olivia, knuth));
		assertTrue(sn.isConnected(raph, ally));
		assertTrue(sn.isConnected(raph, olivia));
		assertTrue(sn.isConnected(raph, knuth));
		assertTrue(sn.isConnected(jebus,knuth));
		assertTrue(sn.isConnected(raph, chris));
		assertTrue(sn.isConnected(olivia, ally));
		assertTrue(sn.isConnected(dan,ben));
		assertTrue(sn.isConnected(baz, dan));
		assertTrue(sn.isConnected(scarlett, ben));
		assertFalse(sn.isConnected(ponting, ally));
		assertFalse(sn.isConnected(ponting, ponting));
		//assertFalse(sn.isConnected(olivia,olivia));
		
	}
	
	public final void testdegreeofSep() {
		raph.addFriend("jesus");
		jebus.addFriend("ally");
		jebus.addFriend("scarlett");
		scarlett.addFriend("jesus");
		jebus.addFriend("olivia");
		olivia.addFriend("Donald");
		chris.addFriend("Olivia");
		baz.addFriend("chris");
		scarlett.addFriend("ben");
		raph.addFriend("dan");
		dan.addFriend("raph");
		SocialNetwork sn = new SocialNetwork(this.people);
		assertEquals(1,sn.getDegreesOfSeparation(jebus, scarlett));
		assertEquals(2,sn.getDegreesOfSeparation(jebus, knuth));
		assertEquals(3,sn.getDegreesOfSeparation(dan, olivia));
		 
		 
	

/*
			SocialNetwork sn = new SocialNetwork();
			sn.addPerson(chris);
			sn.addPerson(ally);
			sn.addPerson(olivia);
			sn.addPerson(ponting);
			sn.addPerson(knuth);
			sn.addPerson(scarlett);
			sn.addPerson(jebus);
	 
			sn.makeFriends(chris, ally);
			sn.makeFriends(chris, scarlett);
			chris.addFriend(olivia.getName());
			ally.addFriend(olivia.getName());
			olivia.addFriend(jebus.getName());
			sn.makeFriends(jebus, knuth);
			ponting.addFriend(knuth.getName());
			ponting.addFriend(scarlett.getName());
			knuth.addFriend(scarlett.getName());
	 
			assertEquals(0, sn.getDegreesOfSeparation(chris, chris));
			assertEquals(1, sn.getDegreesOfSeparation(chris, ally));
			assertEquals(3, sn.getDegreesOfSeparation(chris, knuth));
			assertEquals(-1, sn.getDegreesOfSeparation(ally, ponting));*/
		
	
	}
	
}
