package assign1;

import junit.framework.TestCase;
import org.junit.Before;
import java.util.*;
public class PersonTestCase extends TestCase{
	String [] temp = {"poop","eating poop"};
	Person raph;
	Person chris;
	Person ally;
	Person olivia;
	Person ponting;
	Person knuth;
	Person scarlett;
	Person jebus;

	@Before
	public void setUp() throws Exception {
		super.setUp();
		raph = new Person("Raph",1990,temp);
		chris = new Person("Chris", 1987, temp);
		ally = new Person("Ally", 1986, temp);
		olivia = new Person("Olivia", 2008, temp);
		ponting = new Person("Ricky", 1974, temp);
		knuth = new Person("Donald", 1938, temp); 
		scarlett = new Person("Scarlett", 1984, temp);
		jebus = new Person("Jesus", 0, temp);
	}
	
	public void testgetName() {
		assertEquals("raph", raph.getName());	
	}

	public void testaddFriendandFriends() {
		raph.addFriend("baz");
		assertEquals("baz",raph.getFriendsNames().get(0));
		raph.addFriend("shu");
		raph.addFriend("shu");// check if double are added
		assertEquals("shu",raph.getFriendsNames().get(1));
		assertEquals(2,raph.getFriendsNames().size());// check if double are added
		
		String [] friends = {"hi","piss"};
		raph.addFriends(friends);
		assertEquals("hi",raph.getFriendsNames().get(2));
		
		raph.getFriendsNames().clear();
		
		raph.addFriends(friends);
		assertEquals("hi",raph.getFriendsNames().get(0));
		
		raph.addFriend("boo");
		assertEquals("boo",raph.getFriendsNames().get(2));
	}
	
	public void testremoveFriend() {
		raph.addFriend("Chris");
		raph.removeFriend("Chris");
		raph.removeFriend("blasdlfkajdslfk");
		assertEquals(0,raph.getFriendsNames().size());
		
	}
	
	public void testaddInterest() {
		assertEquals("poop",raph.getInterest().get(0));
		assertEquals("eating poop",raph.getInterest().get(1));
		raph.addInterest("poop3");
		raph.addInterest("poop");
		assertEquals("poop3",raph.getInterest().get(2));
		assertEquals(3,raph.getInterest().size());
	}
	
	public void testremoveInterest() {
		raph.removeInterest("no interest");
		raph.removeInterest("eating poop");
		assertEquals(1, raph.getInterest().size());
		raph.addInterest("eating poop");
	}
	
	public void testisFriendsWith() {
		raph.addFriend("Chris");
		assertTrue(raph.isFriendsWith(chris));
		raph.removeFriend("Chris");
		assertFalse(raph.isFriendsWith(chris));
	}
	
}
