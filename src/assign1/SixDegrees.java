package assign1;


/**
 * @author mac
 */

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SixDegrees {

	private static ArrayList<Person> loadPersonData(String fname) {
		ArrayList<Person> plist = new ArrayList<Person>();
		try {
			FileReader fin = new FileReader(fname);
			Scanner in = new Scanner(fin);
			// read in the data from a file
			/* 
			 * the format of the file is
			 * <personname>
			 * <yearofbirth>
			 * <listofinterests>
			 * <listoffriends>
			 * ---
			 * <personname>
			 * <yearofbirth>
			 * <listofinterests>
			 * <listoffriends>
			 * etc.
			 * <personname> is just a String
			 * <yearofbirth> must be an integer
			 * <listofinterests> must be a comma-separated list of Strings
			 * <listoffriends> must be a comma-separated list of Strings
			 * 
			 */
			String line = null;
			while (in.hasNext()) {
				line = in.nextLine();
				if (line.length() == 0) {
					System.err.println("Missing name");
				}
				String name = line;
				if (!in.hasNext()) {
					System.err.println("EOF reached while scanning age");
					return plist;
				}
				int yob = in.nextInt();
				if (!in.hasNext()) {
					System.err.println("EOF reached while scanning interests");
					return plist;
				}
				in.nextLine();
				String [] interests = in.nextLine().split(",");
				Person p = new Person(name, yob, interests);
				String [] friends = in.nextLine().split(",");
				p.addFriends(friends);
				plist.add(p);
				if (in.hasNext()) {
					line = in.nextLine();	// read the separator line
					if (!line.equals("---")) {
						System.err.println("This line should be \"---\"");
						return plist;
					}
				}
			}
		} catch (NumberFormatException e) {
			System.out.println("Number format failure");
		} catch (FileNotFoundException e) {
			System.out.println("Sorry, I cannot find that file");
		}
		return plist;
	}
	public static void printPeople(List<Person> plist) {
		// TODO print the people in the list in a sensible way
	}
	public static void printNetwork(SocialNetwork sn){
		// TODO print the people in the whole network...
	}
	public static void main(String[] args) {
		//new gui();
		ArrayList<Person> people = loadPersonData("mysocialnetwork.txt");
		SocialNetwork sn = new SocialNetwork(people);
		printNetwork(sn);
		ArrayList<Person> commoners = sn.getFriendsInCommon(sn.getPerson("Nancy Drew"), 
				sn.getPerson("Joe the Plumber"));
		printPeople(commoners);
		/*
		 * There will be other methods used in the automatic marking program,
		 * but this file is provided for your convenience in testing your code.
		 * A sample file "mysocialnetwork.txt" is provided, and you should
		 * write your own too.
		 * The automarker will use several social network data files and call
		 * all of the methods in the SocialNetwork class.  
		 * There will be many tests, designed to show how well your code works,
		 * beginning with simple tests and working up to more complex ones.
		 * Make sure you take full advantage of the opportunity for testing!
		 */
	}

}
